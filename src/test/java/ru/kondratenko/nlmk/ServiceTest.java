package ru.kondratenko.nlmk;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ServiceTest {
    private Service service;

    @BeforeEach
    public void setService() {
        service = new Service();
    }

    @Test
    public void sumCorrect() {
        assertEquals(90, service.sum("45", "45"));
    }

    @Test
    void sumExceptionWrongFormat() {
        assertThrows(IllegalArgumentException.class, () -> service.sum("dfgh", "1"));
    }

    @Test
    void sumExceptionTooBig() {
        assertThrows(IllegalArgumentException.class, () -> service.sum("11111111111111111111111111111", "1"));
    }

    @Test
    public void factorialCorrect() {
        assertEquals(6, service.factorial("3"));
    }

    @Test
    void factorialExceptionNotPositive() {
        assertThrows(IllegalArgumentException.class, () -> service.factorial("-1"));
    }

    @Test
    void factorialExceptionWrongFormat() {
        assertThrows(IllegalArgumentException.class, () -> service.factorial("dfgh"));
    }

    @Test
    void factorialExceptionTooBig() {
        assertThrows(ArithmeticException.class, () -> service.factorial("234567653"));
    }

    @Test
    public void fibonacciCorrect() {
        long[] expected = {1, 1, 2, 3, 5, 8, 13};
        long[] result = service.fibonacci("13");
        assertArrayEquals(expected,result);

        long[] expected1 = {0};
        long[] result1 = service.fibonacci("0");
        assertArrayEquals(expected1,result1);

        long[] expected2 = {1,1};
        long[] result2 = service.fibonacci("1");
        assertArrayEquals(expected2,result2);

        long[] expected3 = {1,1,2};
        long[] result3 = service.fibonacci("2");
        assertArrayEquals(expected1,result1);
    }

    @Test
    void fibonacciExceptionNotPositive() {
        assertThrows(IllegalArgumentException.class, () -> service.fibonacci("-1"));
    }

    @Test
    void fibonacciExceptionWrongFormat() {
        assertThrows(IllegalArgumentException.class, () -> service.fibonacci("dfgh"));
    }

    @Test
    void fibonacciExceptionIsNotFib() {
        assertThrows(IllegalArgumentException.class, () -> service.fibonacci("11"));
    }

    @Test
    void fibonacciExceptionTooBig() {
        assertThrows(ArithmeticException.class, () -> service.fibonacci("1154323687654323567"));
    }

}