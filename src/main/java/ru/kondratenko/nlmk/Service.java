package ru.kondratenko.nlmk;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class Service {
    public static final Logger logger = Logger.getLogger(Service.class.getName());

    public long sum(String arg1, String arg2) throws IllegalArgumentException {
        long arg1L = checkLong(arg1);
        long arg2L = checkLong(arg2);
        return arg1L + arg2L;
    }

    public long factorial(String arg) throws ArithmeticException,IllegalArgumentException {
        long argL = checkPositive(checkLong(arg));
        long result = 1;
        for (long i = 1; i <= argL; i++) {
            result = Math.multiplyExact(result, i);
        }
        return result;
    }

    public long[] fibonacci(String arg) throws IllegalArgumentException, ArithmeticException {
        long argL = isFibonacci(checkPositive(checkLong(arg)));
        List<Long> list = new ArrayList<>();
        if(argL == 0){
            list.add(argL);
        }else{
            long a = 1, b = 1, fib;
            int index = 2;
            list.add(a); list.add(b);
            while((a + b) <= argL ) {
                fib = a + b;
                a = b;
                b = fib;
                list.add(fib);
                index++;
            }
        }
        return  list.stream().mapToLong(l -> l).toArray();
    }

    private long checkLong(String arg) throws IllegalArgumentException {
        return Long.parseLong(arg);
    }

    private long checkPositive(long arg) throws IllegalArgumentException {
        if (arg < 0) {
            logger.severe("Отрицательное число!");
            throw new IllegalArgumentException();
        }
        return arg;
    }

    private static long isFibonacci(long num) throws ArithmeticException,IllegalArgumentException {
        double tmp = 5 * Math.multiplyExact(num, num) - 4;
        double tmp1 = 5 * Math.multiplyExact(num, num) + 4;
        double sq = (long)Math.sqrt(tmp);
        double sq1 = (long)Math.sqrt(tmp1);
        if (!(sq * sq == tmp || sq1 * sq1 == tmp1)) {
            logger.severe("Не раскладывается!");
            throw new IllegalArgumentException();
        }
        return num;
    }
}
