package ru.kondratenko.nlmk;

public class Constants {
    public static final String EXIT = "exit";
    public static final String SUM = "sum";
    public static final String FACTORIAL = "factorial";
    public static final String FIBONACCI = "fibonacci";
}
