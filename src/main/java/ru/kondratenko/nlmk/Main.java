package ru.kondratenko.nlmk;

import java.util.Scanner;
import java.util.logging.Logger;

import static ru.kondratenko.nlmk.Constants.*;

public class Main {

    public static final Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            if (command == null || command.isEmpty()) continue;
            parseCmd(command);
        }
    }

    private static void parseCmd(String s) {
        Service service = new Service();
        String[] cmd = s.split(" ");
        try {
            switch (cmd[0]) {
                case EXIT:
                    System.out.println("Выход...");
                    break;
                case SUM:
                    logger.info(String.valueOf(service.sum(cmd[1], cmd[2])));
                    break;

                case FACTORIAL:
                    logger.info(String.valueOf(service.factorial(cmd[1])));
                    break;

                case FIBONACCI:
                    for (Long item : service.fibonacci(cmd[1])) {
                        logger.info(item.toString());
                    }
                    break;
                default:
                    logger.info("Неизвестная команда!");
                    break;
            }
        } catch (IllegalArgumentException | ArithmeticException e) {
            logger.severe(e.getMessage());
        } catch (Exception ArrayIndexOutOfBoundsException) {
            logger.severe("Слишком много аргументов!");
        }
    }
}
